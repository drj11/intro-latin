# Exercise for Session 1

## Make a font

An assignment artefact is a font file.

You will need the `font8` tools.
Build it with

    make


## System of Modules

i is a base.
C2 symmetry.

- n is i + i (note, still has C2 symmetry)
- m is i + i + i (note, also still has C2 symmetry!)

- u is n with a modified top terminal on the right-hand stroke.
- r is i with additional diagonal square (or n where the right-stroke
  has been reduced to just the diagonal square at the top).

- o is n with the lower right terminal modifed into a stroke
  that is drawn approximately SW to mate with the lower left
  terminal. Then the top is made symmetrically, preserving the
  C2 symmetry.
- v is the top of an n with the bottom of an o. Alternatively,
  an o is using the bottom of the v at both top and bottom.
- w is i + v

- b is **v** with an extended left-stroke, see model.
- l is the left-hand stroke of b
- q is **b** turned upside-down.

- a is **v** turned upside-down with a right-trapezoidal piece
  removed from the left-hand stroke, leaving a lower-right
  triangle as the upper terminal on the lower part of the
  left-hand stroke. This is an accident.
  I was going to reflect the top and add an arc as per the
  model, but since i found this i thought i should try it.

- e is a modified **t**. Spacing is what now?

i:v :: l:b

**v** and **b** share a right-hand component that
can be subtracted from them to yield **i** and **l**
respectively (and also from **w** to yield **n**).
This component does not have a name.

### Shapes that have C2 symmetry:

- i m n o
  


## Fraktur model

The model is partly unclear because the paper is thin and
the scan shows considerable print-through from the reverse side
and the following page.

Observations:

Core module is a simple 1×1 square.
x-height is approximately 5.

The square mostly appears orthogonally
(parallel to the horizontal and vertical of the page), and
at a 45° diagonal.

As drawn the diagonal square is the same size as the orthogonal.

### i

A vertical stroke has a central 3-unit element, with
a diagonal square at both top and bottom.
The diagonal squares just touch y=1 and y=3 lines.
The top diagonal square is shifted left of centre so that its
left side projects out more than the right side,
while still leaving a small projection on the right.
The entire figure has C2 rotational symmetry:
the bottom diagonal square is shifted to the right.

### b

The central 3 unit element of the left-hand stroke of **v** is
extended above with another 3 units and a half unit made with a
45° cut so that the figure occupies the lower-right half.

### x

this is the letter after q on the second row.
It is an r with an addition crossbar and a tick/spike project to
the SW out of lower terminal.

# END
