#!/bin/sh

set -e

if [ "$1" = "-" ]
then
  render () {
    cat
  }
else
  render () {
    kitty icat
  }
fi

fontfile=${1:-*.ttf}

cat << EOT |
minimum
ruin rum
our room
nuovo novo
wow mum
brown romb bob
quoin quill
EOT
  hb-view --output-file=spec11.pdf $fontfile
