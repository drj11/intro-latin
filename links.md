# Links from Font School


## 2022-11-19

(from JV) a font with comparable treatment of serifs:

https://www.fontsmith.com/fonts/fs-kim


## 2022-11-17

Glyphs.app creating a variable font and a virtual primary.


## 2022-10-28

Diacritics resources

Diacritics | Glyphs https://glyphsapp.com/learn/diacritics

How *not* to draw accents - David Jonathan Ross - ATypI 2017
https://www.youtube.com/watch?v=-N--5Wg-2p4

I love Typography (ILT) https://ilovetypography.com/2009/01/24/on-diacritics/

List of pangrams | Clagnut by Richard Rutter https://clagnut.com/blog/2380/

List of resources on designing diacritics | Alphabettes http://www.alphabettes.org/resources-on-designing-diacritics/


## 2022-10-14

On Plantin

https://collection.sciencemuseumgroup.org.uk/documents/aa110126591

https://brigitteschuster.com/media/documents/MonotypePlantin-AdigitalRevival.pdf

https://www.effrapress.co.uk/monotype-mats/#

https://vllg.com/klim/tiempos-text/about


Other mentioned typefaces

http://www.identifont.com/similar?3IO

https://fonts.adobe.com/fonts/bell-centennial-std

https://www.linotype.com/5595379/pmn-caecilia-family.html

https://www.lucasfonts.com/fonts/the-serif

https://frenchtype.com/Capucine

https://typesupply.com/fonts/marigny

## 2022-10-08

from J

https://www.axis-praxis.org/playground/noordzij-cube/

A talk on reviving type process:

https://vimeo.com/279408465?embedded=true&source=vimeo_logo&owner=8580392


## 2022-10-06

Romain du Roi http://luc.devroye.org/fonts-89919.html

Glyphs Corner Components https://glyphsapp.com/learn/reusing-shapes-corner-components

TEFF Collis http://www.teff.nl/fonts/collis/index.html

SangBleu https://www.swisstypefaces.com/fonts/sangbleu-og/#font

Glyphs Spacing https://glyphsapp.com/learn/spacing

## 2022-10-01

Hoefler on Typeface Design https://jonathanhoefler.com/articles/getting-started-with-typeface-design

Hoefler How to Proof https://jonathanhoefler.com/articles/how-to-proof-a-typeface

http://typeworkshop.com/

GrilliType https://www.instagram.com/p/CHsqfJOjsdY/?hl=fr

OhNo Teaching https://ohnotype.co/blog/tagged/teaching

https://typedesignschool.com/

Specimen Books https://library.typographica.org/specimen-books-of-metal-wood-type

Specimen Books at Letterform Archive https://library.typographica.org/specimen-books-of-metal-wood-type

Type Specimens https://typography.guru/forums/topic/44-type-specimens-online/

Letter Library (not working?) https://letterlibrary.org/home/

Typeface Selection https://typographica.org/on-typography/typeface-selection-resources/

Laura Worthington Type Design Resources https://lauraworthingtondesign.com/news/article/type-design-resources

Recommended Books https://typedrawers.com/discussion/1275/recommended-type-design-and-typography-books/p1

## 2022-11-08

https://www.typotheque.com/fonts/opentype_feature_support
