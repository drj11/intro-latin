# Project ideation

The actual homework:

• A simple brief (you can use questions below for help)
• Gather any images and references that may apply
• Start a few sketches on the technique that you're most comfortable with (pen and paper / Illustrator / Glyphs…)
- Try a d e n o s

##

- Aequus, Equidae, Aquine (pun on Equine and so on), Asinin (pun on Asinus)
- Aroche (pun on "a roach" from Witcher, and "roche" = rock in French)
- Angharad
- Apony (sounds like Epona, protector of horses)
- Arion (Greek legend)
- Amycus/Amicus (Amycus is a mythical Centaur)
- Asbolus

rhiannon
arhinni

aliminal

the footed letters:
rifhklxnm

anklix rflink 
alaminx
axiflank
anaflax
aniflex
axflink
afilex
anxilf
axmilk
afrix


## Questions answered by the Brief

(from Malou Verlomme)

What's the brief?
- Revival? (From a book, specimen, vernacular…)
- Specific use? (A typeface for…)
- Theoretical problem? (What would a geometrical fraktur look like?)
- etc.

Tone of voice
- What feeling should the typeface convey?
- If it was a person, how would you describe it?

Public
- How would you describe your target audience?
- What social group?

Competition
- Are there existing typefaces answering the same brief?
- Need to differentiate?

References
- Are there any historical references?
- Other piece of design that matches the intended tone of voice?

Intended use
- print/screen?
- intended sizes?
- paired with other fonts?

Proportions
- Should the fonts be space saving?
- X-Height/Cap height requirements?
- Default figures?

Name
- Any intended name/list of words?

Font family
- How many weights/width, etc.?
- Roman + Italic?

Character-set
- Latin only?
- Basic / Pro?

OpenType features
- What figures style needed (OSF/Lining, Proportional/tabular)
- Alternates characters?

## Some ideas

- lambda badge
- graphic novel font
- "war is harmful" lettering
- something from the linocuts
- something already started?
- zebra / rhino / giraffe texture
- for game rulebooks

Where do i like to see fonts? Film title, credits, packaging,
sleeves for DVDs & LPs, playing cards, boxes.


## The barely starteds

- font-brick a single letter A made from onlong brick slips
- font-avo just a very geometric v
- font-atrigon single regular triangle module chained together
- font-arplop A C E H O in a thin commercial sans
- font-argdeg A H S X made from daggers
- font-amus C E I M N from the jacket for Of Mice and Men
- font-airbed D M O from pencil sketch
- font-a252 E F M O graphic 3 trace scope-like forms
- font-a100 for debugging AppleKitView
- font-186 modular tile pattern
- font-169 A M N barely evolved from 3 triangles
- font-085 extremely non-letter-like ZingBats

Show:

- font-a252
- font-airbed
- font-amus
- font-atrigon
- font-avo
- font-brick

# END
