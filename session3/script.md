# Who

Hi my name is David Jones
and i'm excited to be here to talk to you about
my type design project.

# Why

I've designed a couple of fonts that are somewhere between
graphic display and whacky;
i'm here to gain credibility as a designer of more mainstream fonts.

# 020 packaging etc

- my collection of vernacular packaging (which my partner calls
  "a pile of rubbish").

I like the website fontsinuse and where i like to see fonts used
are:

- on packaging
- on LPs
- on book jackets
- on posters

After:

- browsing my existing linocut lettering

# 030 my barely started projects

- browsing my early-stage unfinished projects

i returned to an idea i had when i made a badge:

# 040 badge

a font inspired by hooves (instead of just a letter).

# 060 horses

# 070 sketches

directions of variation that i have explored a bit:

- detail: from realistic to abstract
- angle/robustness: from chonky to slender

Horse shapes suggest other things we can borrow:
knobbliness of knees, ear, tail, saddle and tack.

# 080 sketching in glyphs

# 090 other fonts

Centaur and Pegasus

# 095 actual font references

- Albertus
- Icone
- Swift
- Nordvest
- Infini


# 100 reverse brief

- new family "inspired by horse hooves"
- feelings: informal, naturalistic, conveyance
- personified: a faithful companion (as a good horse is)
- audience: designers
- for packaging, covers, titles, short displays, and short texts

- print and screen at 5mm and above; limited to perhaps one-page
  of text (as in a blurb or a sidebar)
- Clear proportions: large counters, x-height, generously spaced
- a paired design, with one detailed, one abstract

- languages: Welsh/Slovenian/Maltese + ñ
- characters: punctuation, lower case numbers
- later glyphs: lining numbers, fractions, unicode chess pieces,
  smallcaps, roman numerals, blackboard bold, alchemic
- as few alts and ligas as possible

- Centaur (Rogers) and Pegasus (Wolpe) (haha, jk). But actually
  visually like Albertus (Wolpe), Icone (Frutiger), Swift
  (Unger), and more recently Nordvest (Nina Stössinger), Infini
  (Sandrine Nugue).

# 200 Showing



# END
